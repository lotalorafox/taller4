public class Stackluis{
    private Node head;
    private int size =0;
    public Stackluis(){
        head = null;
    }
    public Stackluis(Object o){
        head = new Node(o);
        size++;
    }
    public void push(Object o){
        if(this.isempty()){
            head = new Node(o);
        }else{
            Node n = new Node(o);
            n.next = head;
            head = n;
        }
        size++;
    }
    public Object top(){
        return head.ob;
    }
    public void pop(){
        if(head.next != null){
            head = head.next;
        }else{
            head = null;
        }
        size--;
    }
    public boolean isempty(){
        if(head == null){
            return true;
        }else{
            return false;
        }
    }
    public boolean isnotempty(){
        if(head != null){
            return true;
        }else{
            return false;
        }
    }
    public void printhead(){
        if(!(this.isempty())){
            System.out.println("head " + head.ob);
        }
    }
    public void printheadline(){
        if(!(this.isempty())){
            System.out.print(head.ob);
        }
    }
    public void print(){
        Node n = head.next;
        while(n != null){
            System.out.print(n.ob + " ");
            n = n.next;
        }
        System.out.println();
    }
    public int findsteps(Object s){
        int i=0;
        if(this.isnotempty()){
            Node n = head;
            while(true){
                if(n.ob == s){
                    return i;
                }else{
                    i++;
                    if(n.next == null){
                        break;
                    }else{
                        n = n.next;
                    }
                }
            }
        }
        return i;
    }
    public int getsize(){
        return size;
    }

}
